(function () {
  'use strict'

  suite('Morze decode', function () {
    test('should define decodeMorze function', function () {
      assert.equal(typeof decodeMorze, 'function')
    })

    test('should decode \'SOS\' correctly', function () {
      let result = decodeMorze('SOS')
      assert.equal(result, '...---...')
    })

    test('should decode \'HEY JUDE\' correctly', function () {
      let result = decodeMorze('HEY JUDE')
      assert.equal(result, '.....-.--   .---..--...')
    })
  })
})()

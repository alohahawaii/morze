/**
 * Декодирует строку на латинице в морзе
 *
 * @param {String} text - строка на латинице
 * @returns {string} - декодированная строка морзе
 */
function decodeMorze (text) {
  let symbols = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ .,?:-!'
  let morse = [
    '.-', '-...', '-.-.', '-..', '.', '..-.',
    '--.', '....', '..', '.---', '-.-', '.-..',
    '--', '-.', '---', '.--.', '--.-', '.-.',
    '...', '-', '..-', '...-', '.--', '-..-',
    '-.--', '--..', '.-', '-...', '-.-.', '-..',
    '.', '..-.', '--.', '....', '..', '.---',
    '-.-', '.-..', '--', '-.', '---', '.--.',
    '--.-', '.-.', '...', '-', '..-', '...-',
    '.--', '-..-', '-.--', '--..', '   ', '.-.-.-',
    '--..--', '..--..', '---...', '-....-', '!'
  ]
  let decodedString = ''
  let textLength = text.length
  let symbolsLength = symbols.length

  for (let count = 0; count < textLength; count += 1) {
    let textChar = text.charAt(count)
    for (let i = 0; i < symbolsLength; i += 1) {
      if (textChar === symbols.charAt(i)) {
        decodedString += morse[i]
        break
      }
    }
  }
  return decodedString
}

/**
 * Обработчик формы
 */
document.addEventListener('DOMContentLoaded', function () {
  let form = document.getElementById('form')

  form.addEventListener('submit', function (e) {
    e.preventDefault()

    let inputElementValue = this.getElementsByTagName('textarea')[0].value.trim()
    let outputElement = document.getElementById('output')

    outputElement.value = decodeMorze(inputElementValue)
  })
})
